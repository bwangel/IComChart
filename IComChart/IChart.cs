﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace IComChart
{
    public interface IChart
    {
        void ApplySettings(ChartSettings settings);

        Control ChartControl { get; }

        void SaveImage(Stream stream, ImageFormat format);
    }
}
