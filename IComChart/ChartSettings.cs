﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace IComChart
{
    public class ChartSettings
    {
        public object DataSource { get; set; }

        public string Title { get; set; }

        public string SubTitle { get; set; }

        public IList<ComSeries> Series { get; set; }

        public ComLegend Legend { get; set; }

    }
}
