﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;

namespace IComChart
{
    public partial class CtrlComChart : UserControl, IChart
    {
        public CtrlComChart()
        {
            InitializeComponent();

            _chartLib = ChartFactory.CreateChart();
            Control ctrl = _chartLib.ChartControl;
            ctrl.Dock = DockStyle.Fill;
            this.Controls.Add(ctrl);
        }

        public void ApplySettings(ChartSettings settings)
        {
            _chartLib.ApplySettings(settings);
        }

        IChart _chartLib;

        public void SaveImage(Stream stream, ImageFormat format)
        {
            _chartLib.SaveImage(stream, format);
        }


        public Control ChartControl
        {
            get { return _chartLib.ChartControl; }
        }
    }
}
