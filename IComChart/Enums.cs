﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IComChart
{
    public enum ChartType
    {
        Default = 0,
        Column,
        Column3D,
        Bar,
        Line,
        Line3D,
        Spline,
        Pie,
        Pie3D,
    }
}
