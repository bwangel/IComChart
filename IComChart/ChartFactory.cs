﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;

namespace IComChart
{
    class ChartFactory
    {
        public static IChart CreateChart()
        {
            string clsSetting = ConfigurationManager.AppSettings["Chart"];
            if (!String.IsNullOrEmpty(clsSetting))
            {
                return (IChart)LoadClass(clsSetting);
            }
            else
            {
                return new StdChart();
            }
        }

        /// <summary>
        /// 通过反射生成指定类型对象
        /// </summary>
        /// <param name="fullClassName">类型全称</param>
        /// <returns></returns>
        public static object LoadClass(string fullClassName)
        {
            if (fullClassName == null) return null;
            string[] names = fullClassName.Split(new char[] { ',' }, 2);

            if (names.Length == 1)
            {
                int i = fullClassName.LastIndexOf('.');
                string path = fullClassName.Substring(0, i);
                return Assembly.Load(path).CreateInstance(fullClassName);
            }
            else if (names.Length == 2)
            {
                return Assembly.Load(names[1]).CreateInstance(names[0], true);
                //return Assembly.GetEntryAssembly().CreateInstance(names[0]);
            }
            else
            {
                return null;
            }
        }

    }
}
