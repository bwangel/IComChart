﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace IComChart
{
    public class ComLegend
    {
        public ContentAlignment Alignment { get; set; }

        public bool Visible { get; set; }
    }
}
