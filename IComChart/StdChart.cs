﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace IComChart
{
    /// <summary>
    /// 使用.net标准的图表组件的接口实现
    /// </summary>
    class StdChart : IChart
    {
        Chart _chart;
        public StdChart()
        {
            _chart = new Chart();
            ChartArea chartArea1 = new ChartArea();
            chartArea1.Name = "ChartArea1";
            chartArea1.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX2.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY2.MajorGrid.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisX.Interval = 1D;
            chartArea1.AxisX2.Interval = 1D;
            chartArea1.AxisX.LabelAutoFitMaxFontSize = 10;
            chartArea1.AxisX.LabelAutoFitMinFontSize = 8;

          //  _chart.AntiAliasing = AntiAliasingStyles.Graphics;
            _chart.ChartAreas.Add(chartArea1);
        }

        public void ApplySettings(ChartSettings settings)
        {
            if (settings.Series == null) return;
            if (settings.Legend != null)
            {
                _chart.Legends.Clear();
                if (settings.Legend.Visible)
                {
                    var legend = _chart.Legends.Add("图例");
                    switch (settings.Legend.Alignment)
                    {
                        case ContentAlignment.TopRight:
                            legend.Docking = Docking.Right;
                            legend.Alignment = StringAlignment.Near;
                            break;
                        default:
                            legend.Docking = Docking.Bottom;
                            legend.Alignment = StringAlignment.Center;
                            break;
                    }
                }
            }

            _chart.Titles.Clear();
            // Add a title to the chart (if necessary).
            if (!String.IsNullOrEmpty(settings.Title))
            {
                _chart.Titles.Add(settings.Title).Font
                    = new System.Drawing.Font("微软雅黑", 12F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(134)));
            }

            _chart.Series.Clear();
            foreach (var ser in settings.Series)
            {
                var series = new Series();
                if (String.IsNullOrEmpty(ser.Name))
                {
                    series.Name = ser.YDataMembers;
                }
                else
                {
                    series.Name = ser.Name;
                }
                series.ChartType = GetEnumType(ser.ChartType);
                if (ser.ChartType.ToString().Contains("3D"))
                {
                    _chart.ChartAreas[0].Area3DStyle.Enable3D = true;
                }
                series.XValueMember = ser.XDataMember;
                series.YValueMembers = ser.YDataMembers;
                _chart.Series.Add(series);
            }

            _chart.DataSource = settings.DataSource;
        }

        public Control ChartControl
        {
            get { return _chart; }
        }

        public void SaveImage(System.IO.Stream stream, System.Drawing.Imaging.ImageFormat format)
        {
            _chart.SaveImage(stream, format);
        }

        SeriesChartType GetEnumType(ChartType type)
        {
            switch (type)
            {
                case ChartType.Line3D:
                case ChartType.Line: return SeriesChartType.Line;
                case ChartType.Spline: return SeriesChartType.Spline;
                case ChartType.Pie3D:
                case ChartType.Pie: return SeriesChartType.Pie;
                case ChartType.Bar: return SeriesChartType.Bar;
                default: return SeriesChartType.Column;
            }
        }
    }
}
