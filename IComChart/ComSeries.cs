﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IComChart
{
    public class ComSeries
    {
        public string Name { get; set; }

        public ChartType ChartType { get; set; }

        public string XDataMember { get; set; }

        public string YDataMembers { get; set; }
    }
}
