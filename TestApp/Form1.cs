﻿using IComChart;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace TestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static DataTable GetTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("公司", typeof(String));
            dt.Columns.Add("测试数", typeof(double));
            dt.Columns.Add("数据2", typeof(double));
            dt.Rows.Add("联想", 34.45, 56.83);
            dt.Rows.Add("Microsoft", 3.42, 79.2);
            dt.Rows.Add("淘宝网", 12.38, 33.34);
            dt.Rows.Add("阿里巴巴", 25.91, 58.01);
            dt.Rows.Add("金山", 31.8, 32.5);
            dt.Rows.Add("软件园", 41.8, 9.51);
            dt.Rows.Add("博览财经", 36.8, 9.01);
            dt.Rows.Add("拓尔思", 15.8, 19.01);
            dt.Rows.Add("中建康城", 21.8, 29.01);

            return dt;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var settings = new ChartSettings()
             {
                 Series = new ComSeries[]
                 {
                     new ComSeries()
                     {
                         ChartType = ChartType.Column,
                         XDataMember = "公司",
                         YDataMembers = "测试数"
                     },
                     new ComSeries()
                     {
                         ChartType = ChartType.Column,
                         XDataMember = "公司",
                         YDataMembers = "数据2"
                     },
                 },

                 DataSource = GetTable(),
                 Title = "图表测试展示",
                 Legend = new ComLegend()
                 {
                     Alignment = ContentAlignment.BottomCenter,
                     Visible = true
                 }
             };

            try
            {
                ctrlComChart1.ApplySettings(settings);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
