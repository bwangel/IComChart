﻿using DevExpress.XtraCharts;
using IComChart;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DevChart
{
    class DevChart : IChart
    {
        ChartControl _chart;

        public Control ChartControl
        {
            get { return _chart; }
        }

        public ChartSettings Settings
        {
            get;
            set;
        }

        public DevChart()
        {
            _chart = new ChartControl();
        }

        public void ApplySettings(ChartSettings settings)
        {
            if (settings.Series == null) return;

            _chart.Series.Clear();
            foreach (var ser in settings.Series)
            {
                var series = new DevExpress.XtraCharts.Series(ser.Name, GetEnumType(ser.ChartType));
                series.ArgumentDataMember = ser.XDataMember;
                series.ValueDataMembers.AddRange(ser.YDataMembers);
                if (String.IsNullOrEmpty(series.Name))
                {
                    series.Name = series.ValueDataMembers[0];
                }
                _chart.Series.Add(series);
            }

            if (settings.Legend != null)
            {
                _chart.Legend.Visible = settings.Legend.Visible;
                switch (settings.Legend.Alignment)
                {
                    case ContentAlignment.TopRight:
                        _chart.Legend.AlignmentHorizontal = LegendAlignmentHorizontal.RightOutside;
                        _chart.Legend.AlignmentVertical = LegendAlignmentVertical.Top;
                        _chart.Legend.Direction = LegendDirection.TopToBottom;
                        break;
                    default:
                        _chart.Legend.AlignmentHorizontal = LegendAlignmentHorizontal.Center;
                        _chart.Legend.AlignmentVertical = LegendAlignmentVertical.BottomOutside;
                        _chart.Legend.Direction = LegendDirection.LeftToRight;
                        break;
                }
            }

            // Add a title to the chart (if necessary).
            if (!String.IsNullOrEmpty(settings.Title))
            {

                ChartTitle chartTitle1 = new ChartTitle();
                chartTitle1.Text = settings.Title;
                _chart.Titles.Add(chartTitle1);
            }

            // Hide the legend (if necessary).
            //     sideBySideBarChart.Legend.Visible = false;

            // Rotate the diagram (if necessary).
            //((XYDiagram)sideBySideBarChart.Diagram).Rotated = true;

            // Add the chart to the control.
            //_chart.Dock = DockStyle.Fill;
            //this.Controls.Add(_chart);
            _chart.DataSource = settings.DataSource;
        }

        ViewType GetEnumType(ChartType type)
        {
            switch (type)
            {
                case ChartType.Bar: return ViewType.Bar3D;
                case ChartType.Line: return ViewType.Line;
                case ChartType.Spline: return ViewType.Spline;
                case ChartType.Pie: return ViewType.Pie;
                case ChartType.Pie3D: return ViewType.Pie3D;
                case ChartType.Line3D: return ViewType.Line3D;
                default: return ViewType.Bar;
            }
        }

        public void SaveImage(System.IO.Stream stream, System.Drawing.Imaging.ImageFormat format)
        {
            _chart.ExportToImage(stream, format);
        }
    }
}
